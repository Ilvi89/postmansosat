import React, {useEffect, useState} from 'react'
import {over} from 'stompjs';
import SockJS from 'sockjs-client';

var stompClient = null;
const EventObserver = () => {
    const [userData, setUserData] = useState({
        dossier: '',
        receivername: '',
        connected: false,
        message: '',
        photosCount: 0,
        photoIds: []
    });
    useEffect(() => {
        console.log(userData);
    }, [userData]);

    const connect = () => {
        let Sock = new SockJS('http://localhost:8080/ws');
        stompClient = over(Sock);
        stompClient.connect({}, onConnected, onError);
    }

    const onConnected = () => {
        setUserData({...userData, "connected": true});
        stompClient.subscribe('/user/' + userData.dossier + '/photos', onPhotos);
    }

    const onPhotos = (payload) => {
        var payloadData = JSON.parse(payload.body);
        // console.log(payloadData)
        setUserData(prevState => {
            return {...prevState, "photoIds": [...prevState.photoIds, payloadData.id]}
        })
        console.log(payloadData.id)
    }



    const onError = (err) => {
        console.log(err);
    }
    const handleDossier = (event) => {
        const {value} = event.target;
        setUserData({...userData, "dossier": value});
    }
    const registerUser = () => {
        connect();
    }
    const Photo = (id) =>  <div>{"Photo " + {id}}</div>

    return (
        <div className="container">
            {userData.connected ?
                <div className="chat-box">
                    <ol>
                        {userData.photoIds.map((id, index) => (
                            <div key={index}>Photo {id}</div>
                        ))}
                    </ol>
                </div>
                :
                <div className="register">
                    <input
                        id="user-name"
                        placeholder="Enter your dossier"
                        name="userDossier"
                        value={userData.dossier}
                        onChange={handleDossier}
                        margin="normal"
                    />
                    <button type="button" onClick={registerUser}>
                        connect
                    </button>
                </div>}
        </div>
    )
}

export default EventObserver
