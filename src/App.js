import React from 'react'
import EventObserver from './components/EventObserver'

const App = () => {
  return (
    <EventObserver />
  )
}

export default App;

